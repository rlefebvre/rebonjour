{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, ... }:
  let
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
  in
  {
    # https://discourse.nixos.org/t/use-buildinputs-or-nativebuildinputs-for-nix-shell/8464
    #  nativeBuildInputs -> tools needed to produce the package (such as compilers)
    #  buildInputs -> tools needed by the runtime
    devShell.x86_64-linux = pkgs.mkShell {
      buildInputs = with pkgs; [
        nodejs-18_x
      ];

      shellHook = ''
          export PUPPETEER_EXECUTABLE_PATH=${pkgs.chromium.outPath}/bin/chromium
      '';
    };
  };
}
