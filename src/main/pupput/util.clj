(ns pupput.util
  #_(:require
   [clojure.core.async :refer [go]]))

; it would be useful to have a config to turn logging on/off
(defmacro go-safe
  "trap errors, wrap in ex-info"
  [& body]
  `(cljs.core.async/go
     (try
       ~@body
       (catch js/Error err#
         (do
           (let [err# (if (and (instance? cljs.core/ExceptionInfo err#)
                                (= (:error (ex-data err#) :go-safe)))
                         (ex-cause err#)
                         err#)]
             ;(js/console.log "[go-safe]" err#)
             (taoensso.timbre/warn "[go-safe]" err#)
             (ex-info "go-safe channel produced error" {:error :go-safe} err#)))))))

(defmacro <?
  "reads from a channel, rethrow exceptioninfo"
  [ch]
  `(let [v# (cljs.core.async/<! ~ch)]
     (if (and
          (instance? cljs.core/ExceptionInfo v#)
          ;(= (:error (ex-data v#)) :promise-error)
          )
       (throw v#)
       v#)))


