(ns pupput.util
  (:require-macros [pupput.util])
  (:require
   [cljs.core.async :as async :refer [go <!]]
   [cljs.core.async.interop :refer [p->c] :refer-macros [<p!]]
   [taoensso.timbre :as timbre :refer [info debug trace]]
   ["path" :as path]
   ["fs" :as fs-]
   ["fs/promises" :as fs]))

;; puppeteer helpers

(def current-page)
(defn new-page [browser url]
  ; open a new page with custom download path
  (pupput.util/go-safe
   (let [page (<p! (.newPage browser))]
     (set! current-page page)
     ;(<p! (-> page (._client)
     ;         (.send "Page.setDownloadBehavior"
     ;                #js {:behavior "allow" :downloadPath constants/download-path})))

     (<p! (.goto page url))
     (println "new-page" url)
     page)))

(defn ->text* [elem]
  (pupput.util/go-safe
   {:element elem :text (<p! (.jsonValue (<p! (.getProperty elem "textContent"))))}))

(defn ->text [elem]
  (pupput.util/go-safe
    (:text (<! (->text* elem)))))

(defn find-with-text [parent selector re]
  (let [;->text
        ;(fn [el]
        ;  (trace "[->text]" el)
        ;  (pupput.util/go-safe
        ;   {:element el
        ;    :text (<p! (.jsonValue (<p! (.getProperty el "textContent"))))}))
        ]
    (pupput.util/go-safe
     (let [elements
           (->> (<p! (.$$ parent selector))
                (map ->text*)
                (async/map vector)
                <!)
           _ (trace "[find-with-text]" selector (count elements) elements)
           matches (->> elements (filter #(re-find re (:text %))))]
       (trace "[find-with-text] matches" matches)
       (-> matches first :element)))))

(defn select-by-text [page selector re]
  (pupput.util/go-safe
   (let [option (<! (find-with-text page (str selector " > option") re))
         _ (trace "[select-by-text]" selector re option)
         option-value (<p! (.jsonValue (<p! (.getProperty option "value"))))
         _ (<p! (.select page selector option-value))]
     page)))

(defn wait-click [page selector & [{:keys [timeout]
                                    :or {timeout 30000}}]]
  (pupput.util/go-safe
   (<p! (.waitForSelector page selector #js {:timeout timeout}))
   (<p! (.click page selector))))

(defn nav [page url]
  (pupput.util/go-safe
   (let [pall
         (js/Promise.all
          #js [(.waitForNavigation page #js {:waitUntil #js ["load" "domcontentloaded" "networkidle0"]})
               (.goto page url)])
         results (<p! pall)]
     (trace "[nav]" url results)
     (first results))))

(defn click-nav [page selector]
  (pupput.util/go-safe
   (let [pall
         (js/Promise.all
           #js [(.waitForNavigation page #js {:waitUntil #js ["load" "domcontentloaded" "networkidle0"]})
                (.click page selector)])
         _ (trace "[click-nav] (sel=" selector ") wait for navigation" pall)
         results (<p! pall)]
     (trace "[click-nav]" selector results)
     (first results)))

  ; (pupput.util/go-safe
  ;   (let [results (async/map
  ;                  vector
  ;                  [(p->c (.waitForNavigation page))
  ;                   (p->c (.click page selector))])
  ;         [nav click] (<! results)]
  ;     (println "[click-nav]" nav click)
  ;     nav))
  )

(defn wait-click-nav [page selector]
  (pupput.util/go-safe
   (<p! (.waitForSelector page selector))
   (trace "selector ready" selector)
   (<p! (.hover page selector))
   (pupput.util/<? (click-nav page selector))))

(defn click-nav-element [page element]
  (pupput.util/go-safe
   (let [pall (js/Promise.all
               #js [(.waitForNavigation page #js {:waitUntil #js ["load" "domcontentloaded" "networkidle0"]})
                    (.click element)])
         _ (trace "[click-nav-element] wait")
         results (<p! pall)]
     _ (trace "[click-nav-element] results" results)
     (first results))))

;hover, wait, click
(defn hover-click [page selector & [timeout]]
  (pupput.util/go-safe
   (<p! (.waitForSelector page selector))
   (<p! (.hover page selector))
   (<p! (.waitForTimeout page (or timeout 1000)))
   (<p! (.click page selector))
   ;(<p! (.waitForTimeout page 1000))
   ))

;; filesystem helpers

(defn file-recent? [filename]
  (go
    (let [stat (<p! (fs/lstat filename))
          age (- (.getTime (js/Date.)) (.-birthtimeMs stat))]
      {:filename filename :recent? (< age 10000)})))

(defn most-recent
  "returns the most recently created file in the specified directory"
  [directory]
  (go
    (->> (<! (->> (<p! (fs/readdir directory))
                  (map #(.resolve path directory %))
                  (map file-recent?)
                  (async/map vector)))
         (filter :recent?)
         (map :filename)
         first)))

(comment
  ; we want to watch for a max time
  (pupput.util/go-safe
   (println
    (fs/watch "foo" ))))

(defn wait-for-pattern [dir re]
  (pupput.util/go-safe
    (let [notify-chan (async/chan)
          timeout-chan (async/timeout 60000)

          watcher (fs-/watch
                    dir
                    #js {}
                    (fn [event-type filename*]
                      ;(trace "[fs/watch]" event-type filename*)
                      (when-let [matches? (and (re-find re filename*) (= "change" event-type))]
                        (trace "[fs/watch]" event-type filename*)
                        (async/put! notify-chan [event-type filename*]))))
          _ (trace "watching dir" dir "for" re)
          [v port] (async/alts! [timeout-chan notify-chan])
          timeout? (= port timeout-chan)
          _ (trace "[fs/watch] alts returned" v port timeout?)]
      (.close watcher)
      (when timeout?
        (trace "timeout waiting for activity in directory:" dir)
        (throw (ex-info "timeout waiting for file" {:error :timeout :re re})))
      (trace "[fs/watch] returning" v)
      v)))

(defn wait-for-file [fullname]
  (let [dir (.dirname path fullname)
        filename (.basename path fullname)
        re (re-pattern (str filename "$"))]
    (wait-for-pattern dir re)))

(defn click-wait-for-pattern [page click-selector dir re]
  (pupput.util/go-safe
    (let [wait-file (wait-for-pattern dir re)
          ;; TODO are there timing garantees that we will be watching before actually clicking?
          wait-click (<! (p->c (.click page click-selector)))
          results (pupput.util/<? wait-file)]
      (trace "[utils][click-wait-for-pattern]" results)
      results)))

(defn click-wait-for-file [page click-selector filename]
  (pupput.util/go-safe
    (let [wait-ch (wait-for-file filename)
          _ (<! (p->c (.click page click-selector)))
          results (pupput.util/<? wait-ch)]
      (trace "[utils][click-wait-for-file]" results)
      results)))

(comment
  (js/console.log (fs-/watch))
  (js/console.log fs-/FSWatcher)
  (macroexpand-1 '(pupput.util/go-safe 1))
  (macroexpand-1 '(pupput.util/<? ch))

  )
