(ns rebonjour.core
  (:require
   [cljs.core.async :as async :refer [go <!]]
   [cljs.core.async.interop :refer [p->c] :refer-macros [<p!]]

   [taoensso.timbre :as timbre :refer [info debug]]

   [pupput.util :as u :refer [go-safe <? wait-click-nav click-nav wait-click]]
   [rebonjour.notif]
   [rebonjour.conf :refer [conf]]

   ;["path" :as path]
   ;["fs/promises" :as fs]
   ["puppeteer" :as puppeteer]
   ["puppeteer-extra" :as puppeteer-extra]
   ["puppeteer-extra-plugin-stealth" :as puppeteer-extra-plugin-stealth]
 ))

(timbre/set-min-level! :trace)

(def test? false)
(def period 45000)

(def browser)
(def current-page)

(defn init-browser []
  (.use puppeteer-extra (puppeteer-extra-plugin-stealth))
  (go-safe
   (set! browser
     (<p! (.launch puppeteer-extra
                   #js {:executablePath (.executablePath puppeteer)
                        :headless false})))))

(defn get-frame [page frame-name]
  (go-safe
   (<p! (.waitForFrame
         page
         (fn [frame]
           (let [equal? (= frame-name (.name frame))]
             (js/Promise.resolve equal?)))))))

(comment
  (init-browser))

(defn fill-form [page]
  (go-safe
    (let [page (<? (get-frame page "iframeHubClinicPostalCode"))

          {:keys [ramq seqno first-name last-name]} conf

          sel-ramq "#healthInsuranceNumber"
          sel-seqno "#healthInsuranceNumberSequence"
          sel-first-name "#firstName"
          sel-last-name "#lastName"
          sel-confirm-check "#confirmation-checkbox-input"
          sel-confirm "#confirm > span"

          _ (<p! (.waitForSelector page sel-ramq))
          _ (info "ramq selector ready")
          _ (<p! (.type page sel-ramq ramq))
          _ (<p! (.type page sel-seqno seqno))
          _ (<p! (.type page sel-first-name first-name))
          _ (<p! (.type page sel-last-name last-name))
          _ (<p! (.click page sel-confirm-check))
          _ (<p! (.click page sel-confirm))

          _ (<? (wait-click-nav page "#continue"))])))

(defn search [page]
  (go-safe
    (let [frm (<? (get-frame page "iframeHubClinicPostalCode"))
          sel-postal "#postalCode"
          sel-type "#search-form > div > app-input-field > label > div.input-group > div > select"
          sel-confirm "#confirm"

          _ (<p! (.waitForSelector frm sel-postal))
          _ (<p! (.type frm sel-postal (:postal-code conf)))
          _ (<p! (.select frm sel-type "Urgent"))
          _ (<? (wait-click frm sel-confirm))
          _ (<p! (.waitForFunction frm "document.querySelector(\".loading-container\") === null"))

          not-found (<p! (.$ frm "#availabilitiesNotFoundMessage"))
          results (<p! (.$ frm ".results-container"))
          _ (info "results ready" not-found results)]
      (not not-found))))

(defn return [page]
  (go-safe
   (let [frm (<? (get-frame page "iframeHubClinicPostalCode"))]
     (<? (wait-click frm "app-new-search-button")))))

(defn auto-book [page]
  (go-safe
   (let [frm (<? (get-frame page "iframeHubClinicPostalCode"))]
     (info "accepting first available appointment")
     (<? (wait-click frm "app-locked-walkin-availability button")))))

(defn init-bonjour [browser]
  (go-safe
   (let [url "https://bonjour-sante.ca/uno/hubcodepostal"
         page (<p! (.newPage browser))
         _ (set! current-page page)
         _ (<p! (.goto page url))
         sel-cookies-accept "#didomi-notice-agree-button > span"
         _ (<p! (.waitForSelector page sel-cookies-accept))
         _ (<p! (.click page sel-cookies-accept))]
     page)))

(defn booking [page]
  (go-safe
    (let [frm (<? (get-frame page "iframeHubClinicPostalCode"))
          sel-registration-form "#registration-form"

          {:keys [phone email]} conf

          reason "28" ;Autre"
          sel-cell "#cellPhone"
          sel-home "#homePhone"
          sel-email "#email"
          sel-reasons "#reasons"
          sel-confirm "#confirm"

          ;; wait for user to choose booking
          _ (<p! (.waitForSelector frm sel-registration-form #js {:timeout 200000}))
          _ (<p! (.type frm sel-cell phone))
          _ (<p! (.type frm sel-home phone))
          _ (<p! (.type frm sel-email email))
          _ (<p! (.select frm sel-reasons reason))
          _ (<p! (.click frm sel-confirm))]
      (when-not test?
        (<p! (.click frm "lib-base-dialog button"))))))

(defn rebonjour []
  (go-safe
   (let [_ (<! (rebonjour.conf/init))
         transport (rebonjour.notif/init-transport)
         ;_ (rebonjour.notif/send-email transport {:subject "test" :text "allo"} )
         browser (<? (init-browser))
         page (<? (init-bonjour browser))]

     (<? (fill-form page))

     (loop []
       (let [found? (<? (search page))]
         (if found?
           (do
             (info "got a result, send notif")
             (rebonjour.notif/send-email
               transport
               {:subject "Appointments available !!"
                :text "Found something'"}))
           (do
             (info "nothin', sleep and recur")
             (<p! (.waitForTimeout page period))
             (<? (return page))
             (recur)))))
     
     (when (:auto-book? conf) (<? (auto-book page)))

     ; auto book once user has picked the time
     (<? (booking page))
     (info "done"))))

(defn main []
  (rebonjour))

