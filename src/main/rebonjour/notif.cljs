(ns rebonjour.notif
  (:require ["nodemailer" :as nodemailer]
            [pupput.util :as u :refer [go-safe <? click-nav]]
            [cljs.core.async.interop :refer [p->c] :refer-macros [<p!]]
            [taoensso.timbre :as timbre :refer [info debug]]
            [rebonjour.conf :refer [conf]]))

(defn init-transport []
  (.createTransport
    nodemailer
    #js {:host "smtp.gmail.com"
         :port 587
         :auth #js  {:user (-> conf :gmail :user)
                     :pass (-> conf :gmail :pass)}}))

(defn send-email [transport {:keys [subject text]}]
  (go-safe
    (let [from (str "rebonjour <" (-> conf :gmail :user) ">")
          to (-> conf :email)
          html (str "<h1>" text "</h1>")]
      (<p! (.sendMail transport
                      #js {:from from :to to :subject subject :text text :html html})))))
