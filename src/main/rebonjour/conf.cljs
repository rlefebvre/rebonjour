(ns rebonjour.conf
  (:require [cljs.reader :as reader]
            [cljs.core.async :as async :refer [go <!]]
            [cljs.core.async.interop :refer [p->c] :refer-macros [<p!]]
            ["fs/promises" :as fs]))

(def conf)

(defn init []
  (go
    (-> "conf.edn"
        (fs/readFile "utf8")
        (<p!)
        reader/read-string
        (as-> $ (set! conf $)))))

;(init)

